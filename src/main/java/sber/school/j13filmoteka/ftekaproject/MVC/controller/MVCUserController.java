package sber.school.j13filmoteka.ftekaproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import sber.school.j13filmoteka.ftekaproject.config.JWTTokenUtil;
import sber.school.j13filmoteka.ftekaproject.dto.UserDTO;
import sber.school.j13filmoteka.ftekaproject.service.UserService;
import sber.school.j13filmoteka.ftekaproject.service.userdetails.CustomUserDetailsService;

import static sber.school.j13filmoteka.ftekaproject.constants.UserRolesConstants.ADMIN;

@Slf4j
@Hidden
@Controller
@RequestMapping("/users")
public class MVCUserController {

    private final UserService userService;

    private final JWTTokenUtil jwtTokenUtil;
    private final CustomUserDetailsService customUserDetailsService;

    public MVCUserController(UserService userService, JWTTokenUtil jwtTokenUtil, CustomUserDetailsService customUserDetailsService) {
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.customUserDetailsService = customUserDetailsService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserDTO userDTO,
                               BindingResult bindingResult) {
        if (userDTO.getLogin().equalsIgnoreCase(ADMIN) || userService.getUserByLogin(userDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже существует");
            UserDetails foundUser = customUserDetailsService.loadUserByUsername(userDTO.getLogin());
            String token = jwtTokenUtil.generateToken(foundUser);
            System.out.println("admin token: " + token);
            return "registration";
        }
        if (userService.getUserByEmail(userDTO.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже существует");
            return "registration";
        }
        userService.create(userDTO);
        return "redirect:login";
    }
}
