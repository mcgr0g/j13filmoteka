package sber.school.j13filmoteka.ftekaproject.repository;

import org.springframework.stereotype.Repository;
import sber.school.j13filmoteka.ftekaproject.model.Director;

@Repository
public interface DirectorRepository
    extends GenericRepository<Director>{
}
