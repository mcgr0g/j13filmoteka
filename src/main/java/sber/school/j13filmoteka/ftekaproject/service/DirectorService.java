package sber.school.j13filmoteka.ftekaproject.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import sber.school.j13filmoteka.ftekaproject.dto.DirectorDTO;
import sber.school.j13filmoteka.ftekaproject.dto.DirectorWithFilmsDTO;
import sber.school.j13filmoteka.ftekaproject.mapper.DirectorMapper;
import sber.school.j13filmoteka.ftekaproject.mapper.DirectorsWithFilmsMapper;
import sber.school.j13filmoteka.ftekaproject.model.Director;
import sber.school.j13filmoteka.ftekaproject.model.Film;
import sber.school.j13filmoteka.ftekaproject.repository.DirectorRepository;
import sber.school.j13filmoteka.ftekaproject.repository.FilmRepository;

import java.util.List;

@Service
public class DirectorService
    extends GenericService<Director, DirectorDTO> {

    private final DirectorRepository directorRepository;
    private final FilmRepository filmRepository;

    private final DirectorsWithFilmsMapper directorsWithFilmsMapper;
    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              FilmRepository filmRepository,
                              DirectorsWithFilmsMapper directorsWithFilmsMapper) {
        super(directorRepository, directorMapper);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
        this.directorsWithFilmsMapper = directorsWithFilmsMapper;
    }

    public Director getDirectorEntity(Long id) {
        return directorRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Директора по id: " + id + " не найдено"));
    }

    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Director director = getDirectorEntity(directorId);
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильма по id: " + filmId + " не найдено"));
        director.getFilms().add(film);
        return mapper.toDTO(repository.save(director));
    }

    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms() {
        return directorsWithFilmsMapper.toDTOs(repository.findAll());
    }

}
