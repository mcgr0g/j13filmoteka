package sber.school.j13filmoteka.ftekaproject.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sber.school.j13filmoteka.ftekaproject.dto.DirectorDTO;
import sber.school.j13filmoteka.ftekaproject.model.Director;
import sber.school.j13filmoteka.ftekaproject.service.DirectorService;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссеры",
        description = "Контроллер для работы с режисеррами фильмов из фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController
        extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }


    @Operation(description = "добавить фильм режиссеру", method = "addFilmToDirector")
    @RequestMapping(
            value = "/add/{filmId:[\\d+]}/to/{directorId:[\\d+]}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<DirectorDTO> addFilmToDirector(
            @PathVariable(value = "filmId") Long filmId,
            @PathVariable(value = "directorId") Long directorId) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(directorService.addFilm(directorId, filmId));
    }
}
