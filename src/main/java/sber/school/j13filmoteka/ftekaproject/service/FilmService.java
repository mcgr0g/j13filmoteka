package sber.school.j13filmoteka.ftekaproject.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import sber.school.j13filmoteka.ftekaproject.dto.FilmDTO;
import sber.school.j13filmoteka.ftekaproject.dto.FilmsWithDirectorsDTO;
import sber.school.j13filmoteka.ftekaproject.mapper.FilmMapper;
import sber.school.j13filmoteka.ftekaproject.mapper.FilmsWithDirectorsMapper;
import sber.school.j13filmoteka.ftekaproject.model.Director;
import sber.school.j13filmoteka.ftekaproject.model.Film;
import sber.school.j13filmoteka.ftekaproject.repository.DirectorRepository;
import sber.school.j13filmoteka.ftekaproject.repository.FilmRepository;

import java.util.List;

@Service
public class FilmService
    extends GenericService<Film, FilmDTO> {

    private final FilmRepository repository;

    private final DirectorRepository directorRepository;

    private final FilmsWithDirectorsMapper filmsWithDirectorsMapper;

    protected FilmService(FilmRepository repository,
                          FilmMapper mapper,
                          DirectorRepository directorRepository,
                          FilmsWithDirectorsMapper filmsWithDirectorsMapper) {
        super(repository, mapper);
        this.repository = repository;
        this.directorRepository = directorRepository;
        this.filmsWithDirectorsMapper = filmsWithDirectorsMapper;
    }

    public Film getFilmEntity(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Данных для id: " + id + " не найдено"));
    }

    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = getFilmEntity(filmId);
        Director director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Директора по id: " + directorId + " не найдено"));
        film.getDirectors().add(director);
        return mapper.toDTO(repository.save(film));
    }

    public List<FilmsWithDirectorsDTO> getAllFilmsWithDirectors() {
        return filmsWithDirectorsMapper.toDTOs(repository.findAll());
    }

}
