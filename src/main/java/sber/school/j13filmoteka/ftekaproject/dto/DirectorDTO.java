package sber.school.j13filmoteka.ftekaproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDTO
        extends GenericDTO {

    private String fio;

    private int position;

    private Set<Long> filmsIds;
}
