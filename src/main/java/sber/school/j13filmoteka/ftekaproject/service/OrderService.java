package sber.school.j13filmoteka.ftekaproject.service;


import org.springframework.stereotype.Service;
import sber.school.j13filmoteka.ftekaproject.dto.OrderDTO;
import sber.school.j13filmoteka.ftekaproject.mapper.OrderMapper;
import sber.school.j13filmoteka.ftekaproject.model.Order;
import sber.school.j13filmoteka.ftekaproject.repository.OrderRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository,
                        OrderMapper orderMapper) {
        super(orderRepository, orderMapper);
        this.orderRepository = orderRepository;
    }

    public List<OrderDTO> userOrders(Long userId, boolean activeRentOnly) {
        List<Order> orderList = orderRepository.findByUserIdAndPurchase(userId, true);
        List<Order> orderListOfRent;
        if (activeRentOnly) {
            orderListOfRent = orderRepository.findByUserIdAndRentPeriodEndBeforeNow(userId, LocalDateTime.now());
        } else {
            orderListOfRent = orderRepository.findByUserIdAndPurchase(userId, false);
        }
        orderList.addAll(orderListOfRent);
        return mapper.toDTOs(orderList);
    }
}
