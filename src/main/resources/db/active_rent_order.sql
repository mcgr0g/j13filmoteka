select
    o.rent_date + (o.rent_period|| ' days')::interval "rent_end"
    ,o.*
from orders o
where 1=1
and o.user_id = 10
and o.purchase = false
and o.rent_date + (o.rent_period|| ' days')::interval >= now();
;