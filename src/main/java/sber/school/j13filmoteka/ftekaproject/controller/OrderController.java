package sber.school.j13filmoteka.ftekaproject.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sber.school.j13filmoteka.ftekaproject.dto.OrderDTO;
import sber.school.j13filmoteka.ftekaproject.model.Order;
import sber.school.j13filmoteka.ftekaproject.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/orders")
@Tag(name = "Заказы на фильмы", description = "Контроллер по работке с заказами фильмов пользователями фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderController
        extends GenericController<Order, OrderDTO> {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }

    @Operation(description = "вернуть все арендованный/купленные фильмы пользователя", method = "getAllUserOrders")
    @RequestMapping(
            value = "getAllUserOrders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<OrderDTO>> getAllUserOrders(@RequestParam(value = "userId") Long userId,
                                                           @RequestParam(value = "active") boolean active) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.userOrders(userId, active));
    }
}
