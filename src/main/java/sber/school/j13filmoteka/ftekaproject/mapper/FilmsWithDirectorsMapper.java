package sber.school.j13filmoteka.ftekaproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.j13filmoteka.ftekaproject.dto.FilmsWithDirectorsDTO;
import sber.school.j13filmoteka.ftekaproject.model.Film;
import sber.school.j13filmoteka.ftekaproject.model.GenericModel;
import sber.school.j13filmoteka.ftekaproject.repository.DirectorRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmsWithDirectorsMapper
        extends GenericMapper<Film, FilmsWithDirectorsDTO> {

    private final DirectorRepository directorRepository;

    protected FilmsWithDirectorsMapper(ModelMapper mapper,
                                       DirectorRepository directorRepository){
        super(mapper, Film.class, FilmsWithDirectorsDTO.class);
        this.directorRepository = directorRepository;
    }


    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmsWithDirectorsDTO.class)
                .addMappings(m -> m.skip(FilmsWithDirectorsDTO::setDirectorsIds))
                .setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(FilmsWithDirectorsDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmsWithDirectorsDTO source, Film destination) {
        destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorsIds())));
    }

    @Override
    protected void mapSpecificFields(Film source, FilmsWithDirectorsDTO destination) {
        destination.setDirectorsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getDirectors().stream()
                    .map(GenericModel::getId)
                    .collect(Collectors.toSet());
    }
}
