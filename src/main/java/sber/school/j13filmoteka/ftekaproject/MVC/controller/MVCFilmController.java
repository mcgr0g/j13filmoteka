package sber.school.j13filmoteka.ftekaproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sber.school.j13filmoteka.ftekaproject.dto.DirectorDTO;
import sber.school.j13filmoteka.ftekaproject.dto.FilmDTO;
import sber.school.j13filmoteka.ftekaproject.dto.FilmsWithDirectorsDTO;
import sber.school.j13filmoteka.ftekaproject.service.DirectorService;
import sber.school.j13filmoteka.ftekaproject.service.FilmService;

import java.util.List;

@Hidden
@Controller
@RequestMapping("films")
public class MVCFilmController {

    private final FilmService filmService;

    private final DirectorService directorService;

    public MVCFilmController(FilmService filmService,
                             DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<FilmsWithDirectorsDTO> result = filmService.getAllFilmsWithDirectors();
        List<DirectorDTO> directorsAvailableList = directorService.listAll();
        model.addAttribute("films", result);
        model.addAttribute("directorsAvailableList", directorsAvailableList);
        return "films/viewAllFilms";
    }

    @GetMapping("add")
    public String create(){
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO){
        filmService.create(filmDTO);
        return "redirect:/films";
    }

    @RequestMapping(
            value = "/append/{directorId}/to/{filmId}",
            method = RequestMethod.POST
    )
    public String addDirectorToFilm(
            @PathVariable(value = "filmId") Long filmId,
            @PathVariable(value = "directorId") Long directorId) {
        filmService.addDirector(filmId, directorId);
        return "redirect:/films";
    }
}
