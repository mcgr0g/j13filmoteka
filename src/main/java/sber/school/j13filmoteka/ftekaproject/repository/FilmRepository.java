package sber.school.j13filmoteka.ftekaproject.repository;

import org.springframework.stereotype.Repository;
import sber.school.j13filmoteka.ftekaproject.model.Film;

@Repository
public interface FilmRepository
        extends GenericRepository<Film> {
}
