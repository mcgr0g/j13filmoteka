package sber.school.j13filmoteka.ftekaproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import sber.school.j13filmoteka.ftekaproject.dto.UserDTO;
import sber.school.j13filmoteka.ftekaproject.model.GenericModel;
import sber.school.j13filmoteka.ftekaproject.model.User;
import sber.school.j13filmoteka.ftekaproject.repository.OrderRepository;
import sber.school.j13filmoteka.ftekaproject.utils.DateFormatter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private final OrderRepository orderRepository;

    public UserMapper(ModelMapper modelMapper,
                      OrderRepository orderRepository) {
        super(modelMapper, User.class, UserDTO.class);
        this.orderRepository = orderRepository;
    }

    @Override
    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setUserOrderIds))
                .setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders))
                .setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getUserOrderIds())) {
            destination.setOrders(new HashSet<>(
                    orderRepository.findAllById(source.getUserOrderIds())
            ));
        } else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setUserOrderIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                    .map(GenericModel::getId)
                    .collect(Collectors.toSet());
    }
}
