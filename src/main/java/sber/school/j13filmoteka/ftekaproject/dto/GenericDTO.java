package sber.school.j13filmoteka.ftekaproject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class GenericDTO {
    private Long id;

    private String createdBy;

    private LocalDateTime createdAt;

//    private boolean isDeleted;
//
//    private LocalDateTime deletedAt;
//
//    private String deletedBy;
}
