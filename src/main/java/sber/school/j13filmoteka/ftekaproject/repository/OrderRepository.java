package sber.school.j13filmoteka.ftekaproject.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sber.school.j13filmoteka.ftekaproject.model.Order;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OrderRepository
        extends GenericRepository<Order> {

    List<Order> findByUserIdAndPurchase(Long userId, boolean isPurchase);

    @Query(value = "select o.* from Orders o where o.user_id = ?1 " +
            "and o.purchase = false " +
            "and o.rent_date + (o.rent_period|| ' days')\\:\\:interval >= ?2"
            , nativeQuery = true)
    List<Order> findByUserIdAndRentPeriodEndBeforeNow(Long userId, LocalDateTime rentPeriodEnd);

}
