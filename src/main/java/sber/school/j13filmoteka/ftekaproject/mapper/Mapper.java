package sber.school.j13filmoteka.ftekaproject.mapper;

import sber.school.j13filmoteka.ftekaproject.dto.GenericDTO;
import sber.school.j13filmoteka.ftekaproject.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    D toDTO(E entity);

    E toEntity(D dto);

    List<D> toDTOs(List<E> entitiesList);

    List<E> toEntities(List<D> dtoList);
}
