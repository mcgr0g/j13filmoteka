package sber.school.j13filmoteka.ftekaproject.repository;

import org.springframework.stereotype.Repository;
import sber.school.j13filmoteka.ftekaproject.model.User;

@Repository
public interface UserRepository
    extends GenericRepository<User> {

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User findUserByChangePasswordToken(String token);
}
