package sber.school.j13filmoteka.ftekaproject.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import sber.school.j13filmoteka.ftekaproject.dto.GenericDTO;
import sber.school.j13filmoteka.ftekaproject.mapper.GenericMapper;
import sber.school.j13filmoteka.ftekaproject.model.GenericModel;
import sber.school.j13filmoteka.ftekaproject.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    protected final GenericRepository<T> repository;

    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericService(GenericRepository<T> repository,
                          GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public N getOne(final Long id){
        return mapper.toDTO(repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Данных для id: " + id + " не найдено"))
        );
    }

    public List<N> listAll(){
        return mapper.toDTOs(repository.findAll());
    }

    public N create(N object){
        object.setCreatedAt(LocalDateTime.now());
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public N update(N object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
