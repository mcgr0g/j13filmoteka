package sber.school.j13filmoteka.ftekaproject.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import sber.school.j13filmoteka.ftekaproject.dto.RoleDTO;
import sber.school.j13filmoteka.ftekaproject.dto.UserDTO;
import sber.school.j13filmoteka.ftekaproject.mapper.UserMapper;
import sber.school.j13filmoteka.ftekaproject.model.User;
import sber.school.j13filmoteka.ftekaproject.repository.UserRepository;

import java.time.LocalDateTime;

@Slf4j
@Service
public class UserService extends GenericService<User, UserDTO> {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository, userMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    @Override
    public UserDTO create(UserDTO userDTO) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        userDTO.setRole(roleDTO);
        userDTO.setCreatedBy("REGISTRATION FORM");
        userDTO.setCreatedAt(LocalDateTime.now());
        userDTO.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(userDTO)));
    }
}
