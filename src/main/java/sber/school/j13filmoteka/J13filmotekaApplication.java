package sber.school.j13filmoteka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J13filmotekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(J13filmotekaApplication.class, args);
    }

}
