package sber.school.j13filmoteka.ftekaproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sber.school.j13filmoteka.ftekaproject.model.Film;
import sber.school.j13filmoteka.ftekaproject.model.User;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO
        extends GenericDTO {

    private Long filmId;

    private Long userId;

    private LocalDateTime rentDate;

    private Integer rentPeriod;

    private Boolean purchase;

}
