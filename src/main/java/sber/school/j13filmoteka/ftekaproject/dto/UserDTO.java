package sber.school.j13filmoteka.ftekaproject.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDTO
        extends GenericDTO {

    private String login;

    private String password;

    private String email;

    private String birthDate;

    private String phone;

    private String firstName;

    private String lastName;

    private String middleName;

    private String address;

    private RoleDTO role;

    private String changePasswordToken;

    private Set<Long> userOrderIds;
}
