package sber.school.j13filmoteka.ftekaproject.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import sber.school.j13filmoteka.ftekaproject.dto.FilmDTO;
import sber.school.j13filmoteka.ftekaproject.model.Director;
import sber.school.j13filmoteka.ftekaproject.model.Film;
import sber.school.j13filmoteka.ftekaproject.repository.DirectorRepository;
import sber.school.j13filmoteka.ftekaproject.repository.FilmRepository;
import sber.school.j13filmoteka.ftekaproject.repository.GenericRepository;
import sber.school.j13filmoteka.ftekaproject.service.FilmService;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами в фильмотеке")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FilmController
        extends GenericController<Film, FilmDTO> {

    private final FilmService filmService;

    public FilmController(FilmService filmService) {
        super(filmService);
        this.filmService = filmService;
    }

    @Operation(description = "добавить режисера фильму", method = "addDirectorToFilm")
    @RequestMapping(
            value = "/add/{directorId}/to/{filmId}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FilmDTO> addDirectorToFilm(
            @PathVariable(value = "filmId") Long filmId,
            @PathVariable(value = "directorId") Long directorId) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(filmService.addDirector(filmId, directorId));
    }
}
