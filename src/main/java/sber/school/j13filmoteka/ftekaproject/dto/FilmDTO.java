package sber.school.j13filmoteka.ftekaproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sber.school.j13filmoteka.ftekaproject.model.Genre;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO
        extends GenericDTO {

    private String title;

    private Integer premierYear;

    private String country;

    private Genre genre;

    private Set<Long> directorsIds;
}
