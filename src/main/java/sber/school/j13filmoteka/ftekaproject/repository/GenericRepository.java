package sber.school.j13filmoteka.ftekaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import sber.school.j13filmoteka.ftekaproject.model.GenericModel;

@NoRepositoryBean
public interface GenericRepository<T extends GenericModel>
    extends JpaRepository<T, Long> {
}
